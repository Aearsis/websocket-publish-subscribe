FROM python
RUN pip install aioconsole websockets
COPY *.py /

EXPOSE 8000
CMD python server.py
