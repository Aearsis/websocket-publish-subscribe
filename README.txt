## Websocket publisher-subscriber middleware

A simple script to broadcast event log across subscribers.

### Usage

Server is a simple script, `server.py`, and listens on port 8000.

Client accepts one argument, and that is URL of the websocket it should connect
to. It is more of a demo to test the functionality.

### Deployment

Preferrably via docker:

```
docker build . -t ws_pubsub
docker run --detach --rm -p 8000:8000 ws_pubsub
```
