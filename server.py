#!/usr/bin/env python3

import asyncio
import datetime
import json
import logging
import os
import signal
import sqlite3
import sys
import websockets

logging.basicConfig(
    format="%(asctime)s %(message)s",
    level=logging.INFO,
)

started_on = datetime.datetime.now()

class Channel:
    def __init__(self):
        self.head = self.tail = asyncio.Future()

    def publish(self, value):
        tail, self.tail = self.tail, asyncio.Future()
        tail.set_result((value, self.tail))

    def finish(self, value):
        fut = asyncio.Future()
        fut.cancel()
        self.tail.set_result((value, fut))

    def clear(self):
        self.head = self.tail

    async def subscribe(self):
        tail = self.head
        while True:
            value, tail = await tail
            yield value

    __aiter__ = subscribe

class Server:
    def __init__(self):
        self.channel = Channel()
        self.queue = asyncio.Queue()
        self.task = None
        self.stopped = True

        path = os.environ.get('DB_PATH', 'server.db3')
        self.db = sqlite3.connect(path)
        self.db.execute("PRAGMA journal_mode = wal")
        self.db.execute("CREATE TABLE IF NOT EXISTS log(seq, message)")

    async def _consumer(self, websocket):
        try:
            async for message in self.channel:
                await websocket.send(message)

                if self.stopped:
                    # socket.close(1000, f"Server going down")
                    # publisher will be cancelled
                    # break
                    pass
        except websockets.exceptions.ConnectionClosedOK:
            pass
        except Exception as e:
            logging.error(f"Client {websocket.id}: exception in consumer", exc_info=e)

    async def handler(self, websocket):
        id = str(websocket.id)
        logging.info(f"Client {id}: connected")

        await websocket.send(json.dumps({
            'welcome': {
                'started': started_on.timestamp(),
                'version': 1,
                'your_id': id,
            },
            'from': 'server',
            'timestamp': datetime.datetime.now().timestamp(),
        }))

        consumer = asyncio.create_task(self._consumer(websocket))

        try:
            async for message in websocket:
                msg = json.loads(message)
                assert type(msg) is dict
                await self.queue.put((msg, datetime.datetime.now(), id))
        except Exception as e:
            logging.error(f"Client {websocket.id}: exception in producer", exc_info=e)
            await websocket.close(4001, f"Exception when reading: {e}")

        try:
            await consumer
        except websockets.exceptions.ConnectionClosed as e:
            pass

        await websocket.wait_closed()

    async def loop(self):
        seq = 0
        self.channel.clear()

        cur = self.db.execute("SELECT seq, message FROM log")
        for seq, message in cur:
            self.channel.publish(message)

        while not self.stopped:
            message, time, id = await self.queue.get()
            seq += 1
            try:
                logging.info(f"{id}: {message}")

                if 'reset' in message and message['reset']:
                    self.db.execute("DELETE FROM log")
                    self.channel.clear()

                event = json.dumps({
                    **message,
                    'seq': seq,
                    'from': id,
                    'timestamp': time.timestamp(),
                })

                if 'exit' in message:
                    self.db.commit()
                    self.channel.publish(event)
                    if id != "server":
                        sys.exit(message['exit'])
                else:
                    self.db.execute("INSERT INTO log VALUES (?, ?)", (seq, event))
                    self.db.commit()
                    self.channel.publish(event)

                # consumers will wake up
            except Exception as e:
                logging.warning("Error when handling message", exc_info=e)
                raise
            finally:
                self.queue.task_done()

    async def run(self):
        self.task = asyncio.create_task(self.loop())
        self.stopped = False
        await self.task

    async def stop(self, **info):
        if not self.stopped:
            logging.info("Stopping")
            self.stopped = True
            await self.queue.put(({ 'exit': 0, **info }, datetime.datetime.now(), 'server'))
        elif self.task is not None:
            logging.info("Killing")
            sys.exit(1)



async def main():
    server = Server()
    loop = asyncio.get_event_loop()

    for sig in [signal.SIGINT, signal.SIGTERM]:
        loop.add_signal_handler(sig, lambda: asyncio.ensure_future(server.stop(signal=sig.name)))

    async with websockets.serve(server.handler, None, 8000):
        await server.run()

if __name__ == "__main__":
    asyncio.run(main())
