#!/usr/bin/env python3

import asyncio
import json
import sys
from aioconsole import *
import websockets


async def consumer(websocket):
    async for message in websocket:
        print(json.loads(message))

async def producer(websocket):
    while True:
        try:
            line = await ainput()

            try:
                payload = json.loads(line)
            except Exception as e:
                print(e)
                payload = { 'message': line }

            await websocket.send(json.dumps(payload))
        except EOFError:
            break

async def main(executable, url=None):
    if url is None:
        print(f"Usage: {executable} <url>")
        print(f"  e.g. {executable} ws://localhost:8000")
        sys.exit(1)

    async with websockets.connect(url) as websocket:
        task = asyncio.create_task(consumer(websocket))
        await producer(websocket)

        task.cancel()

if __name__ == "__main__":
    asyncio.run(main(*sys.argv))
